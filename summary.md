# Complex
---
# Happy path
---
Maybe Niketa [has][have][had] a [nice.and.polite][expensive.and.deep] smile.
---
Maybe Niketa [had.before][have.before][has.before] a [nice.and.polite][expensive.and.deep] smile.

> Кейт има хубава усмивка.
---
# With more than 3 choices 
---
Kate used[had][need][want][require] to smoke but now she runs[run][ran] a lot instead.
---
# With two options
---
Kate used[had] to smoke but now she runs[run] a lot instead.
---
> Кейт има хубава усмивка.
---
# Example 1
A: We work hard ................. the day.
B: Me too, but I finish .............. 2 pm.
---
A: We work hard during[across][throughout] the day.
---
B: Me too, but I finish before[till] 2 pm.
---
# Diference between during and throughout


During time doesn`t work
---
